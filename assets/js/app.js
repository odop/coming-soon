jQuery(document).ready(function($) {
    /* Validation subscribe
    -------------------------------------------------------------------*/
    $("#contact-submit").click(function(){
        var first_name = $("#first_name").val();
        var last_name = $("#last_namel").val();
        var contact_email = $("#contact_email").val();
        var contact_subject = $("#contact_subjectl").val();
        var message = $("#message").val();
        $.ajax({
	       url : 'php/contact.php',
	       type : 'POST',
	       data: {first_name : first_name, last_name : last_name, contact_email : contact_email, contact_subject : contact_subject, message : message},
	       success : function(data){
	           mailchimpResponse(data);
            },
        });
    });
    
    /* Validation contact
    -------------------------------------------------------------------*/
    $("#subscribe-submit").click(function(){
        var email = $("#subscribe-email").val();
        $.ajax({
	       url : 'php/subscribe.php',
	       type : 'POST',
	       data: {contact_email : email},
	       success : function(data){
	           mailchimpResponse(data);
            },
        });
    });
    
    /* Function permettant de gérer le paramètre de retour AJAX
     -------------------------------------------------------------------*/
    function mailchimpResponse(resp) {
        if(resp === 'success') {
            $('.subscribe').hide();
            $('.subscribe-message').html("Merci de vous être inscrit à Odop !");
        } else if(resp === 'email_used') {
            $('.alert-warning').addClass("alert").html("Email déjà utilisé").fadeIn().delay(2000).fadeOut(3000);
        } else if(resp === 'bad_email') {
            $('.alert-warning').addClass("alert").html("L'email renseigné est incorrect").fadeIn().delay(2000).fadeOut(3000);
        }
    };
});