<?php

try{
    $oPdo = new PDO( "mysql:dbname=odop;host=localhost", "odop_user", "NA9qnsPRtrF1GdIv" );
    $oPdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
    $oPdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_OBJ);
}catch(PDOException $e){
    echo 'Connexion impossible';
}


if (isset( $_POST[ 'contact_email' ] ) && filter_var( $_POST[ 'contact_email' ], FILTER_VALIDATE_EMAIL ) ){

    $sEmail = $_POST[ 'contact_email' ];
    $sSQLFindEmail = $oPdo->prepare( "SELECT email FROM suscriber WHERE email = :email" );
    $sSQLFindEmail->execute( [ "email" => $sEmail ] );
    $aResult = $sSQLFindEmail->fetchAll();
    $iNbLigne = count($aResult);
    // Si il n'existe aucune ligne, cela veut dire que le mail n'existe pas
    // Donc on peut l'ajouter tranquillement dans la base de données
    if( $iNbLigne < 1){

        $sSQLInsertEmail = $oPdo->prepare( 'INSERT INTO suscriber (email, date_suscribe) VALUES (:email, NOW())');
        $sSQLInsertEmail->execute( [ "email" => $sEmail ] );
        echo "success";
    }
    else{
        echo "email_used";
    }
}
else{

    echo "bad_email";
}